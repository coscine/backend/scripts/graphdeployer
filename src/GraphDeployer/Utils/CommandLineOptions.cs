﻿using CommandLine;

namespace Coscine.GraphDeployer.Utils;

public static partial class CommandLineOptions
{
    public class GraphDeployerOptions
    {
        [Option("redeploy", Required = false, Default = false, HelpText = "An argument that tells the program to redeploy all graphs.")]
        public bool Redeploy { get; set; }

        [Option("dummy", Required = false, Default = false, HelpText = "An argument that tells the program to execute in dummy mode.")]
        public bool DummyMode { get; set; }
    }
}