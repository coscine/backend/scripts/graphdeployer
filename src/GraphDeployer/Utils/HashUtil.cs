﻿using System.Security.Cryptography;

namespace Coscine.GraphDeployer.Utils;

public static class HashUtil
{
    public static string GetFileHash(string path)
    {
        using SHA256 sha256 = SHA256.Create();
        using FileStream fileStream = File.OpenRead(path);
        return BitConverter.ToString(sha256.ComputeHash(fileStream));
    }
}