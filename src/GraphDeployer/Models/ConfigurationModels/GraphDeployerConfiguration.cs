﻿namespace Coscine.GraphDeployer.Models.ConfigurationModels;

public class GraphDeployerConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "GraphDeployerConfiguration";

    /// <summary>
    /// Value indicating whether the graph deployer is enabled.
    /// </summary>
    public bool IsEnabled { get; init; }

    /// <summary>
    /// Value indicating whether the graph deployer should skip SSL certificate checks.
    /// </symmary>
    public bool SkipSslCheck { get; set; }

    /// <summary>
    /// The working folder where the graph deployer will store the cloned repositories.
    /// </summary>
    public string? WorkingFolder { get; init; }

    /// <summary>
    /// API token for the coscine API.
    /// Requires the administrator role.
    /// </summary>
    public string ApiKey { get; set; } = null!;

    /// <summary>
    /// Address of the Coscine API.
    /// </summary>
    public string Endpoint { get; set; } = null!;

    /// <summary>
    /// The timeout in milliseconds for the API requests.
    /// </summary>
    public int? Timeout { get; set; }

    /// <summary>
    /// Logger configuration settings.
    /// </summary>
    /// <value>
    /// The logger storage configuration settings, or <c>null</c> if not configured.
    /// </value>
    public LoggerConfiguration? Logger { get; init; }

    /// <summary>
    /// GitLab configuration settings.
    /// </summary>
    /// <value>
    /// The GitLab configuration settings, or <c>null</c> if not configured.
    /// </value>
    public GitLabConfiguration GitLab { get; init; } = null!;

    /// <summary>
    /// Represents the configuration settings for the logger.
    /// </summary>
    public record LoggerConfiguration(string? LogLevel, string? LogHome);

    /// <summary>
    /// Represents the configuration settings for GitLab.
    /// </summary>
    public record GitLabConfiguration
    {
        public required string HostUrl { get; init; }
        public required string Token { get; init; }
        public required List<RepositoryConfiguration> Repositories { get; init; }
    };

    public record RepositoryConfiguration
    {
        public required string Name { get; init; }
        public required Uri Url { get; init; }
        public string? Ref { get; init; }
    };
}
