# Graph Deployer

[[_TOC_]] 

## 📝 Overview

The Graph Deployer, intended to be used as a CRON job, is a .NET application designed to manage and deploy RDF graphs. It interacts with configured GitLab repositories to pull RDF graph definitions and updates a centralized repository if changes are detected or redeployment is triggered. The job uses a series of configurations to determine the operational parameters including the execution environment, repository access, and deployment specifications. 

## Getting Started

Clone the repository:

```bash
$ mkdir -p ~/code/git.rwth-aachen.de/coscine/backend/scripts && cd "$_"
$ git clone git@git.rwth-aachen.de:coscine/backend/scripts/graphdeployer.git
```

Open the new folder in code and in a [devcontainer](https://code.visualstudio.com/docs/devcontainers/containers#_open-a-folder-on-a-remote-ssh-host-in-a-container).

Insider the devcontainer, create a development settings file:

```bash
$ cp /workspace/graph-deployer/src/GraphDeployer/appsettings.Example.json /workspace/graph-deployer/src/GraphDeployer/appsettings.Development.json
```

In the `appsettings.Development.json` Replace any `null` value with a fitting value.

### Features
- **Configuration-driven**: Behavior driven by settings defined in configuration files and environment variables (see `appsettings.json`).
- **Supports Dummy Mode**: Can run in a non-operative mode for testing configurations and process flow without affecting live data (CLI arg `--dummy`).
- **Selective Redeployment**: Allows for forced redeployment of graphs regardless of changes (CLI arg `--redeploy`).
- **Logging**: Extensive logging capabilities to track the process and troubleshoot issues.

## ⚙️ Configuration

The deployment script uses a configuration class `GraphDeployerConfiguration` to manage settings such as:
- `IsEnabled`: Toggles the deployer on or off.
- `WorkingFolder`: Specifies the directory where repositories will be cloned.
- `Logger` Configuration: Specifies the logging level and output directory.
- `GitLab` Configuration: Contains information required to access GitLab, including host URL, token, and repository details.

### Example `appsettings.json`
```json
{
  "GraphDeployerConfiguration": {
    "IsEnabled": true,
    "WorkingFolder": "./output/",
    "Logger": {
      "LogLevel": "Information",
      "LogHome": "C:/Logs"
    },
    "GitLab": {
      "HostUrl": "https://gitlab.example.com",
      "Token": "YourTokenHere",
      "Repositories": [
        {
          "Name": "RepositoryName",
          "Url": "https://gitlab.example.com/group/project.git",
          "Ref": "main"
        }
      ]
    }
  }
}
```

## 📖 Usage

To get started with this project, you will need to ensure you have configured and built it correctly. 

1. Execute the built executable (`.exe`)

To use the **GraphDeployer**, execute the main program with appropriate command-line arguments to control its operation.

### Command-Line Arguments

- `--redeploy`: Forces redeployment of all graphs.
- `--dummy`: Runs the deployer in dummy mode, making no actual changes.

### Running the Deployer

```sh
dotnet GraphDeployer.dll --dummy true --redeploy false
``` 
### Deployment Process
1. **Initialization**: Configurations are loaded and services are initialized.
2. **Repository Management**: For each configured repository, the latest content is cloned (`git clone`) into the specified working folder.
3. **Processing**: Turtle files (`*.ttl`) are processed to accumulate RDF graph changes. Accumulation is nedessary, as they may be split across multiple files.
4. **Deployment Decision**: Compares the current graph file hashes against stored versions to decide if deployment is necessary. Redeployment is enforced on graphs that have no changes, whenever the `--redeploy` CLI argument is set (value: `true`). 
5. **Graph Deployment**: Updates are pushed to the central graph repository if changes are detected or redeployment is forced.
6. **Clean-up**: Cleans the working directory after processing each repository. The working directory will not be deleted, only its contents.

### Considerations
- **Security**: Ensure the security of the GitLab token and other sensitive data in configuration files. Furthermore, ensure the validity of the used GitLab token, as it may expire.
- **Performance**: Large graph datasets or numerous repositories may impact performance; consider scaling solutions or optimizing configurations.
- **Error Handling**: Robust error handling and logging are crucial for diagnosing issues during deployments.

## 👥 Contributing

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports or feature requests, or by directly contributing to Coscine's source code. To submit your contribution please follow our [Contributing Guideline](https://git.rwth-aachen.de/coscine/docs/public/wiki/-/blob/master/Contributing%20To%20Coscine.md).

## 📄 License

The current open source repository is licensed under the **MIT License**, which is a permissive license that allows the software to be used, modified, and distributed for both commercial and non-commercial purposes, with limited restrictions (see `LICENSE` file)

> The MIT License allows for free use, modification, and distribution of the software and its associated documentation, subject to certain conditions. The license requires that the copyright notice and permission notice be included in all copies or substantial portions of the software. The software is provided "as is" without any warranties, and the authors or copyright holders cannot be held liable for any damages or other liability arising from its use.

## 🆘 Support

1. **Check the documentation**: Before reaching out for support, check the help pages provided by the team at https://docs.coscine.de/en/. This may have the information you need to solve the issue.
2. **Contact the team**: If the documentation does not help you or if you have a specific question, you can reach out to our support team at `servicedesk@itc.rwth-aachen.de` 📧. Provide a detailed description of the issue you're facing, including any error messages or screenshots if applicable.
3. **Be patient**: Our team will do their best to provide you with the support you need, but keep in mind that they may have a lot of requests to handle. Be patient and wait for their response.
4. **Provide feedback**: If the support provided by our support team helps you solve the issue, let us know! This will help us improve our documentation and support processes for future users.

By following these simple steps, you can get the support you need to use Coscine's services as an external user.

## 📦 Release & Changelog

External users can find the _Releases and Changelog_ inside each project's repository. The repository contains a section for Releases (`Deployments > Releases`), where users can find the latest release changelog and source. Withing the Changelog you can find a list of all the changes made in that particular release and version.  
By regularly checking for new releases and changes in the Changelog, you can stay up-to-date with the latest improvements and bug fixes by our team and community!
